---
title: "Hello World!"
Summary: "My first blog post."
date: 2018-12-07T10:45:06+05:30
# affiliatelink: https://google.com
tags:
- Blog
---

Hi, I'm **Raghav**. I am a sophomore currently doing my undergrad in Computer Science and Engineering. I've been thinking to start my own blog since quite long and today is the day when I am going to register my online presence via this blog! I've opted for Jekyll Blog because I always wanted to have a minimal and clean Blog, earlier I've used WordPress. This Jekyll Blog of mine is markdown based (static) and is very fast to load and deploy. Though I am not that good when it comes to writing but from today, I'll try to write as much as I can and share and improve my knowledge thru this blog and hence will get better.

![Hello](https://media.giphy.com/media/fxsqOYnIMEefC/giphy.gif)

