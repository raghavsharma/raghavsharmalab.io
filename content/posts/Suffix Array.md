---
layout: post
title: Suffix Array # & LCP Array
comments: true
date: 2019-01-23T11:45:06+05:30
tags:
- Algorithms
---
**Suffix Array** is an important Data Structure used widely due to the very high memory consumption needs of Suffix Trees. In this blog post, we will cover up the topic Suffix Array. So, the first question that comes in our mind from Suffix Array is that what is Suffix? Suffix of a string is a substring which is at the end of the string characters. For example if we try to Find all the non empty Suffixes of the string "**Banana**" it will be: 
![Suffix Array 1](https://res.cloudinary.com/rexon/image/upload/v1548678485/suffixarray001_xbbxqm.png)

Now, coming back to the suffix array. Suffix Array is the Array containing all the sufixes substrings in a **Sorted** Order. Or, the Suffix Array is the array having the sorted indices of the suffix substrings. All we need is the indices of the suffixes but not the suffix strings itself. By which we get a compressed array having the sorted indices of all the substrings (Suffixes). That is why it is space efficient alternative to the Suffix Tree. Where as the Suffix Tree is itself a compressed form of Trie Data Structure. After Sorting our suffix Array will be,

![Suffix Array 2](https://res.cloudinary.com/rexon/image/upload/v1548956822/suffix_array_new_ys3cne.png)

Here's the sudo code to generate the Suffix Array of a string

```cpp
int* SuffixArray(string str)
{
	int n = str.length(); 
	vector<pair<string,int>> suffix(n);
	int* idx = new int[n];
	for(int i=0;i<n;i++){
		suffix[i].first = str.substr(i);
		suffix[i].second = i;
	}
	sort(suffix.begin(),suffix.end());
	int* arr = new int[n];
	for(int i=0;i<n;i++){
		arr[i] = suffix[i].second;
	}
	return arr;
}

```

<!-- **LCP Array** or the **Longest Common Prefix** Array is the array which keeps the track of the common characters between two adjacent suffixes.<br>
Let's consider the above mentioned example of string "banana", and build the **lcp array** of the suffix strings of "banana". 
![LCP](https://res.cloudinary.com/rexon/image/upload/v1548956479/LCP_Array_wrbpux.png)
<br>
The point to be noted is that the first index of our lcp array is 0, because it is of -->
