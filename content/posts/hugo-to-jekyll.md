---
title: "Jekyll to Hugo!"
Summary: "Migrating my Blog to Hugo from Jekyll."
date: 2019-11-30T10:45:06+05:30
tags:
- Blog
---

After using Jekyll for over a year I've decided to migrate my personal website from **Jekyll to Hugo**. I always wanted my website to be clean and minimal, hence I opted for Jekyll.
